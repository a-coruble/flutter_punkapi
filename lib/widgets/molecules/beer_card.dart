import 'package:flutter/material.dart';
import 'package:flutter_punkapi/models/models.dart';

class BeerCard extends StatelessWidget {
  final Beer beer;

  BeerCard({this.beer});

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   height: 400,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     mainAxisSize: MainAxisSize.min,
    //     children: <Widget>[
    //       Card(
    //         elevation: 18.0,
    //         shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.all(Radius.circular(10.0))),
    //         child: Image.network(
    //           beer.imageUrl,
    //           fit: BoxFit.contain,
    //         ),
    //         clipBehavior: Clip.antiAlias,
    //         margin: EdgeInsets.all(8.0),
    //       ),
    //       Text(
    //         beer.name,
    //         style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
    //       )
    //     ],
    //   ),
    // );
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Image.network(
                beer.imageUrl,
                fit: BoxFit.contain,
              ),
            ),
            Text(beer.name)
          ],
        ),
      ),
    );
  }
}
