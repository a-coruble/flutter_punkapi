import 'package:flutter/material.dart';

import 'package:flutter_punkapi/models/models.dart';
import 'package:flutter_punkapi/widgets/molecules/beer_card.dart';

class BeerGrid extends StatefulWidget {
  final List<Beer> beers;
  final Function loadMore;

  BeerGrid({@required this.beers, @required this.loadMore});

  @override
  _BeerGridState createState() =>
      _BeerGridState(beers: beers, loadMore: loadMore);
}

// AppBar(
//           title: Text('Posts'),
//         ),
class _BeerGridState extends State<BeerGrid> {
  final List<Beer> beers;
  ScrollController _scrollController;
  bool loading = true;
  final Function loadMore;

  _BeerGridState({@required this.beers, @required this.loadMore});

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        loading = true;
        (loadMore != null) && loadMore();
      });
    }
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: beers.map((beer) => BeerCard(beer: beer)).toList(),
      controller: _scrollController,
    );
  }
}
