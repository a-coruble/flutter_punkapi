import 'package:bloc/bloc.dart';

import 'package:flutter_punkapi/apis/punk_api.dart';
import 'package:flutter_punkapi/bloc/bloc.dart';
import 'package:flutter_punkapi/models/Beer.dart';

class BeerBloc extends Bloc<BeerEvent, BeerState> {
  BeerBloc();

  @override
  BeerState get initialState => BeerUninitialized();

  @override
  Stream<BeerState> mapEventToState(BeerEvent event) async* {
    if (event is FetchBeer) {
      yield BeerLoading();
      try {
        final int page = state.props.length > 0 ? state.props[2] : 1;
        final List<Beer> beers = await PunkApiClient().fetchBeers(page);
        yield BeerLoaded(
            beers: beers,
            page: beers.length == 0 ? page + 1 : page,
            hasReachedMax: beers.length == 0 ? true : false);
      } catch (error) {
        yield BeerError();
      }
    }
  }
}
