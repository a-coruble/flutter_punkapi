import 'package:equatable/equatable.dart';
import 'package:flutter_punkapi/models/Beer.dart';

abstract class BeerState extends Equatable {
  const BeerState();

  @override
  List<Object> get props => [];
}

class BeerUninitialized extends BeerState {}

class BeerError extends BeerState {}

class BeerLoading extends BeerState {}

class BeerLoaded extends BeerState {
  final List<Beer> beers;
  final int page;
  final bool hasReachedMax;

  const BeerLoaded({this.beers, this.hasReachedMax, this.page});

  BeerLoaded copyWith({List<Beer> beers, bool hasReachedMax}) {
    return BeerLoaded(
        beers: beers ?? this.beers,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax);
  }

  @override
  List<Object> get props => [beers, hasReachedMax, page];

  @override
  String toString() {
    return """
      BeersLoaded {beers: $beers, hasReachedMax: $hasReachedMax}
    """;
  }
}
