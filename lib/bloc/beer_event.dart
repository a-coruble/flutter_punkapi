import 'package:equatable/equatable.dart';

abstract class BeerEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchBeer extends BeerEvent {}
