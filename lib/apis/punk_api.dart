import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter_punkapi/models/Beer.dart';

class PunkApiClient {
  static final http.Client _httpClient = new http.Client();
  static final PunkApiClient _punkApiClient = PunkApiClient._internal();

  factory PunkApiClient() {
    return _punkApiClient;
  }

  PunkApiClient._internal();

  Future<List<Beer>> fetchBeers(int page, {int perPage = 20}) async {
    final response = await _httpClient
        .get('https://api.punkapi.com/v2/beers?page=$page&per_page=$perPage');
    if (response.statusCode == 200) {
      final Iterable rawBeers = json.decode(response.body);
      List<Beer> l = rawBeers.map((rawBeer) => Beer.fromJson(rawBeer)).toList();
      return l;
    }
    throw Exception(
        'Failed to load beers with {page: $page, per_page: $perPage}');
  }
}
