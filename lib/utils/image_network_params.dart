import 'package:flutter/material.dart';

class ImageNetworkParams {
  final Key key;
  final double scale;
  final ImageFrameBuilder frameBuilder;
  final ImageLoadingBuilder loadingBuilder;
  final String semanticLabel;
  final bool excludeFromSemantics;
  final double width;
  final double height;
  final Color color;
  final BlendMode colorBlendMode;
  final BoxFit fit;
  final AlignmentGeometry alignment;
  final ImageRepeat repeat;
  final Rect centerSlice;
  final bool matchTextDirection;
  final bool gaplessPlayback;
  final FilterQuality filterQuality;
  final Map<String, String> headers;
  final int cacheWidth;
  final int cacheHeight;

  ImageNetworkParams({
    this.key,
    this.scale = 1.0,
    this.frameBuilder,
    this.loadingBuilder,
    this.semanticLabel,
    this.excludeFromSemantics = false,
    this.width,
    this.height,
    this.color,
    this.colorBlendMode,
    this.fit,
    this.alignment = Alignment.center,
    this.repeat = ImageRepeat.noRepeat,
    this.centerSlice,
    this.matchTextDirection = false,
    this.gaplessPlayback = false,
    this.filterQuality = FilterQuality.low,
    this.headers,
    this.cacheWidth,
    this.cacheHeight,
  });

  Map<String, dynamic> toMap() {
    return {
      'key': key,
      'scale': scale,
      'frameBuilder': frameBuilder,
      'loadingBuilder': loadingBuilder,
      'semanticLabel': semanticLabel,
      'excludeFromSemantics': excludeFromSemantics,
      'width': width,
      'height': height,
      'color': color,
      'blendMode': colorBlendMode,
      'boxFit': fit,
      'alignment': alignment,
      'repeat': repeat,
      'centerSlice': centerSlice,
      'matchTextDirection': matchTextDirection,
      'gaplessPlayback': gaplessPlayback,
      'filterQuality': filterQuality,
      'headers': headers,
      'cacheWidth': cacheWidth,
      'cacheHeight': cacheHeight
    };
  }
}
